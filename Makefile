EBI     = ./HIL_EBI
EBI_OBJ = ./HIL_EBI/obj

CC         =  g++
STD        =  -std=c++11
DEFINES    =  
OPT        = -O -g
DEBUG_OPT  = -DDEBUG
CFLAGS = $(DEBUG_OPT) $(STD) -g

LIB_PATH   = -L/usr/local/lib -L../opencv-2.4.9/release/lib
LIB_OPTION = -lopencv_highgui -lopencv_core -lopencv_imgproc -lm 
INCLUDES   = -I. -I$(EBI) -I../opencv-2.4.9/include/opencv -I../opencv-2.4.9/include

SRCS = 	$(EBI)/ExBsdInpaint*.cpp

OBJS = $(EBI_OBJ)/ExBsdInpaint.o \
	$(EBI_OBJ)/LaPyInpaint.o

all: $(OBJS) $(SRCS) Makefile ./Main.cpp
	${CC} ${CFLAGS} $(INCLUDES) -o inpaint ./Main.cpp $(OBJS) $(LIB_PATH) $(LIB_OPTION)

$(EBI_OBJ)/%.o:$(EBI)/%.cpp
	${CC} ${CFLAGS} $(INCLUDES) -c $(EBI)/${*F}.cpp -o $(EBI_OBJ)/${*F}.o

#$(EBI_OBJ)/ExBsdInpaint.o:$(SRCS)
#$(EBI_OBJ)/%.o:$(EBI)/%.cpp
#	${CC} ${CFLAGS} $(INCLUDES) -o $(EBI_OBJ)/ExBsdInpaint.o $(SRCS)
#	${CC} ${CFLAGS} $(INCLUDES) -c $(EBI)/LaPyInpaint*.cpp -o $(EBI_OBJ)/LaPyInpaint.o

library:
	ar rc libinpaint.a $(OBJS)

clean:
	-rm -f *.a core $(OBJS)
