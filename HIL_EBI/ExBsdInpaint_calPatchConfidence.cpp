#include "ExBsdInpaint.h"

#include <iostream>


//=============================================================================
// get sum of confidence value at a patch which centred at the point p
// ERROR: return value < 0
//=============================================================================
double ExBsdInpaint::calPatchConfidence(
    const cv::Mat& confidence,
    cv::Point p
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::calPatchConfidence()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    double res = 0.0;

    double* c = (double* )confidence.data;

    for(int i = -patchSize/2; i < (patchSize+1)/2; i++) {
        for (int j = -patchSize/2; j < (patchSize+1)/2; j++) {
            res += c[(p.y+i)*confidence.cols+(p.x+j)];
        }
    }

    return res;
}
