#include "ExBsdInpaint.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>


//=============================================================================
// find the most approciate point
// use minimum square error between the target point and its source point
// possibly all pixel in the source data can be a source point
//=============================================================================
cv::Point ExBsdInpaint::findExemplarPoint(//color diff alg.
    const cv::Mat& source,
    const cv::Mat& mask,
    cv::Point p
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::findExemplarPoint()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    cv::Mat lab;
    cv::cvtColor(source, lab, CV_RGB2HSV);
    double mse = -1.0;
    cv::Point msep = cv::Point();

    for(int i = patchSize/2; i <= source.rows - (patchSize+1)/2; i++) {
        for(int j = patchSize/2; j <= source.cols - (patchSize+1)/2; j++) {
            double d = calPatchDist(lab, mask, p, cv::Point(j, i));
            if ((mse < 0.0 && d >= 0.0) || (mse > d && d >= 0.0)) {
                mse = d;
                msep.x = j;
                msep.y = i;
            }
        }
    }

    return msep;
}
