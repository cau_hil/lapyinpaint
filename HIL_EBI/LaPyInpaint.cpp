﻿#include "LaPyInpaint.h"
#include <opencv2/highgui/highgui.hpp>

LaPyInpaint::LaPyInpaint(
		cv::Mat src,
		cv::Mat msk,
		int _numOfLayer)
	: ExBsdInpaint(src, msk),
	  numOfLayer(_numOfLayer)
{
	this->LaplacianImages = new cv::Mat[numOfLayer];
	this->LaplacianMasks = new cv::Mat[numOfLayer];
	this->LaplacianConfs = new cv::Mat[numOfLayer];
	this->sourcePoints = new cv::Point[numOfLayer];
	this->targetPoints = new cv::Point[numOfLayer];
	std::vector<cv::Point>::iterator test;
	//confidence table initialize
}


LaPyInpaint::~LaPyInpaint()
{
}


cv::Mat LaPyInpaint::start(int count = 0)
{
	// Build N Layered Laplacian image pyramid
	for (int i = count; i != 0; i--) {
		buildPyramid();
		cv::Point upperTargetPoint = cv::Point();
		cv::Point upperSourcePoint = cv::Point();
		cv::Point upperConfPoint = cv::Point();
		if (numOfLayer == 1){
			break;
		}
		
		for (int layer = numOfLayer - 1; layer >= 0; layer--){
			if (layer == numOfLayer - 1){//가장 높은 단계일경우 이미지 전체가 타겟,소스,컨피던스, 마스크 에어리어다.
				targetArea = LaplacianImages[layer];
				sourceArea = LaplacianImages[layer];
				maskArea = LaplacianMasks[layer];
				confArea = LaplacianConfs[layer];
			}
			else{//아닐경우 전 단계에서 실제 좌표를 구한다.
				targetArea = LaplacianImages[layer](cv::Rect(upperTargetPoint.x - patchSize, upperTargetPoint.y - patchSize, patchSize * 2 + 1, patchSize * 2 + 1));
				sourceArea = LaplacianImages[layer](cv::Rect(upperSourcePoint.x - patchSize, upperSourcePoint.y - patchSize, patchSize * 2 + 1, patchSize * 2 + 1));
				maskArea = LaplacianMasks[layer](cv::Rect(upperTargetPoint.x - patchSize, upperTargetPoint.y - patchSize, patchSize * 2 + 1, patchSize * 2 + 1));
				confArea = LaplacianConfs[layer](cv::Rect(upperTargetPoint.x - patchSize, upperTargetPoint.y - patchSize, patchSize * 2 + 1, patchSize * 2 + 1));//conf area is same with target area
			}
			std::vector<std::vector<cv::Point>> edge = findEdgeLine(maskArea);//마스크 에어리어에서 엣지라인을 찾는다.
			if (edge.empty()){
				numOfLayer--;
				break;
			}
			cv::Mat tempSourceArea;
			if (layer != numOfLayer - 1){
				cvtColor(sourceArea, tempSourceArea, CV_GRAY2RGB);
			}
			else{
				tempSourceArea = sourceArea;
			}
			targetPoints[layer] = findHighestPriorityPoint(tempSourceArea, maskArea, confArea, edge);//주어진 에어리어에서 가장 높은 순위의 타겟 포인트를 찾는다.
			sourcePoints[layer] = findExemplarPoint(tempSourceArea, maskArea, targetPoints[layer]);//주어진 에어리어에서 소스포인트를 찾는다.
			if (layer == numOfLayer - 1){//만일 가장 높은 단계인경우 한단계 아래의 좌표는 단순히 2배
				upperTargetPoint.x = targetPoints[layer].x * 2;
				upperTargetPoint.y = targetPoints[layer].y * 2;
				upperSourcePoint.x = sourcePoints[layer].x * 2;
				upperSourcePoint.y = sourcePoints[layer].y * 2;
			}
			else if (layer == 0){
				targetPoints[layer].x = (upperTargetPoint.x + targetPoints[layer].x - patchSize);
				targetPoints[layer].y = (upperTargetPoint.y + targetPoints[layer].y - patchSize);
				sourcePoints[layer].x = (upperSourcePoint.x + sourcePoints[layer].x - patchSize);
				sourcePoints[layer].y = (upperSourcePoint.y + sourcePoints[layer].y - patchSize);
			}
			else{//아닐경우 낮은 단계의 실제 좌표 구하는 연산
				upperTargetPoint.x = (upperTargetPoint.x + targetPoints[layer].x - patchSize) * 2;
				upperTargetPoint.y = (upperTargetPoint.y + targetPoints[layer].y - patchSize) * 2;
				upperSourcePoint.x = (upperSourcePoint.x + sourcePoints[layer].x - patchSize) * 2;
				upperSourcePoint.y = (upperSourcePoint.y + sourcePoints[layer].y - patchSize) * 2;
			}
			//upperSourcePoint.x= std::min(upperSourcePoint.x, LaplacianImages[layer].cols - patchSize);
			upperSourcePoint.x = std::max(upperSourcePoint.x, patchSize);
			//upperSourcePoint.y = std::min(upperSourcePoint.y, LaplacianImages[layer].rows - patchSize);
			upperSourcePoint.y = std::max(upperSourcePoint.y, patchSize);
		}
		fillPatch(source, mask, targetPoints[0], sourcePoints[0]);//실제 이미지에 patch한다.
		update(mask, confidence, targetPoints[0], sourcePoints[0]);
		std::cout << i << " tar: " << targetPoints[0] << ", src: " << sourcePoints[0] << std::endl;
	}
	return source;
}

void LaPyInpaint::buildPyramid() {
	source.copyTo(LaplacianImages[0]);
	cv::cvtColor(LaplacianImages[0], LaplacianImages[0], CV_BGR2GRAY);//흑백전환
	mask.copyTo(LaplacianMasks[0]);
	confidence.copyTo(LaplacianConfs[0]);
	
	for (int i = 1; i < numOfLayer; i++){
		if (i == numOfLayer - 1){//제일 높은 단계에서는 원래 이미지(칼라)를 다운샘플링한다.
			downSample(source, LaplacianImages[i], i);
		}
		else{//아닐경우 흑백이미지를 다운샘플링 후 빼기로 차이점을 구한다.
			cv::Mat temp;
			downSample(LaplacianImages[0], temp, i-1);//빼질 이미지를 구한다.
			downSample(LaplacianImages[0], LaplacianImages[i], i);
			upSample(LaplacianImages[i], LaplacianImages[i], temp.cols, temp.rows);//두배만큼 업샘플링
			LaplacianImages[i] = temp - LaplacianImages[i];
			downSample(LaplacianImages[i], LaplacianImages[i], i);//다시 원래크기로 바꾼다.
			
			
		}
		downSample(LaplacianMasks[0], LaplacianMasks[i], i);
		downSample(LaplacianConfs[0], LaplacianConfs[i], i);
	}
	
}

void LaPyInpaint::downSample(const cv::Mat src, cv::Mat& dst, int downLevel){
	int d_l = pow(2, downLevel);
	cv::resize(src, dst, cv::Size2i(src.cols / d_l, src.rows / d_l), 0, 0, cv::INTER_LINEAR);
}

void LaPyInpaint::upSample(const cv::Mat src, cv::Mat& dst, int downLevel){
	int d_l = pow(2, downLevel);
	cv::resize(src, dst, cv::Size2i(src.cols * d_l, src.rows * d_l), 0, 0, cv::INTER_LINEAR);
}

void LaPyInpaint::downSample(const cv::Mat src, cv::Mat& dst, int cols, int rows){
	cv::resize(src, dst, cv::Size2i(cols, rows), 0, 0, cv::INTER_LINEAR);
}

void LaPyInpaint::upSample(const cv::Mat src, cv::Mat& dst, int cols, int rows){
	cv::resize(src, dst, cv::Size2i(cols, rows), 0, 0, cv::INTER_LINEAR);
}