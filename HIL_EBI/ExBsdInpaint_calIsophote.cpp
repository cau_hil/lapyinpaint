#include "ExBsdInpaint.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <cmath>
#include <iostream>


cv::Vec2d ExBsdInpaint::calIsophote(
    const cv::Mat& graySource,
    const cv::Mat& mask,
    cv::Point p
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::calIsophote()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    int dx = 0, dy = 0;
    int cx = 0, cy = 0;

    uchar* m = (uchar*)mask.data;
    uchar* g = (uchar*)graySource.data;

    for(int i = -patchSize/2; i < (patchSize+1)/2; i++) {
        for(int j = -patchSize/2; j < (patchSize+1)/2; j++) {
            if(m[(p.y+i)*mask.cols+(p.x+j+1)] == EXEMPLAR_REGION &&
               m[(p.y+i)*mask.cols+(p.x+j-1)] == EXEMPLAR_REGION) {
                cx += patchSize + 1 - abs(i) - abs(j);
                dx += (patchSize + 1 - abs(i) - abs(j)) *
                      (g[(p.y+i)*graySource.cols+(p.x+j+1)] -
                       g[(p.y+i)*graySource.cols+(p.x+j-1)]);

            }
            if(m[(p.y+i+1)*mask.cols+(p.x+j)] == EXEMPLAR_REGION &&
               m[(p.y+i-1)*mask.cols+(p.x+j)] == EXEMPLAR_REGION) {
                cy += patchSize + 1 - abs(i) - abs(j);
                dy += (patchSize + 1 - abs(i) - abs(j)) *
                      (g[(p.y+i+1)*graySource.cols+(p.x+j)] -
                       g[(p.y+i-1)*graySource.cols+(p.x+j)]);

            }
        }
    }

    if(cx == 0 || cy ==0)
        return cv::Vec2d();
    else
        return cv::Vec2d((double)(dx)/(double)(cx), (double)(dy)/double(cy));
}
