#include "ExBsdInpaint.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <cmath>
#include <iostream>


double ExBsdInpaint::calDataTerm(
    const cv::Mat& source,
    const cv::Mat& mask,
    std::vector<cv::Point>::iterator& idx,
    std::vector<cv::Point>& edge
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::calDataTerm()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    double normalizationFactor = 255.0;
    cv::Mat gray;
    cv::cvtColor(source, gray, CV_RGB2GRAY);

    cv::Vec2d pi = calIsophote(gray, mask, (*idx));
    cv::Vec2d pt = calTangent(idx, edge);
    double res = (((pi[0]*pt[0])+(pi[1]*pt[1]))/(sqrt(pt[0]*pt[0]+ pt[1]*pt[1]) * normalizationFactor));

    return (res > 0? res : -res);
}
