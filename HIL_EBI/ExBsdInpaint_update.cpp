#include "ExBsdInpaint.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>


void ExBsdInpaint::update(
    cv::Mat& mask,
    cv::Mat& confidence,
    cv::Point tar,
    cv::Point src
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::update()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    uchar* m = (uchar*)mask.data;
    double* c = (double* )confidence.data;

    for(int i = -patchSize/2; i < (patchSize+1)/2; i++) {
        for(int j = -patchSize/2; j < (patchSize+1)/2; j++) {
            if(m[(tar.y+i)*mask.cols+(tar.x+j)] != EXEMPLAR_REGION) {
                m[(tar.y+i)*mask.cols+(tar.x+j)] = EXEMPLAR_REGION;
                c[(tar.y+i)*confidence.cols+(tar.x+j)]
                    = calPatchConfidence(confidence, src) / (patchSize*patchSize) * 0.9;
                if(c[(tar.y+i)*confidence.cols+(tar.x+j)] < 0.1)
                    c[(tar.y+i)*confidence.cols+(tar.x+j)] = 0.1;
            }
        }
    }
}
