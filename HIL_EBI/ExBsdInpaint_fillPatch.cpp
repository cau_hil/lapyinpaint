#include "ExBsdInpaint.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> //imread
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>


void ExBsdInpaint::fillPatch(
    cv::Mat& source,
    const cv::Mat& mask,
    cv::Point tar,
    cv::Point src
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::fillPatch()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    cv::Vec3b* s = (cv::Vec3b*)source.data;
    uchar* m = (uchar*)mask.data;

    for(int i = -patchSize/2; i < (patchSize+1)/2; i++) {
        for(int j = -patchSize/2; j < (patchSize+1)/2; j++) {
            if(m[(tar.y+i)*mask.cols+(tar.x+j)] != EXEMPLAR_REGION) {
                s[(tar.y+i)*source.cols+(tar.x+j)] = s[(src.y+i)*source.cols+(src.x+j)];
            }
        }
    }
}
