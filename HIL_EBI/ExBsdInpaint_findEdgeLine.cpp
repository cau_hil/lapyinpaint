#include "ExBsdInpaint.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>


const int HIGH_THRESHOLD = 254;
const int LOW_THRESHOLD = 254;


std::vector<std::vector<cv::Point> > ExBsdInpaint::findEdgeLine(
    const cv::Mat& mask
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::findEdgeLine()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    cv::Mat out;
    std::vector<std::vector<cv::Point> > contours;

    cv::Canny(mask, out, LOW_THRESHOLD, HIGH_THRESHOLD);
    cv::findContours(out, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, cv::Point());

    return contours;
 }
