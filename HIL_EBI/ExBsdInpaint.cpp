#include "ExBsdInpaint.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>


const int WHITE = 255;


ExBsdInpaint::ExBsdInpaint(
    cv::Mat src,
    cv::Mat msk,
    int ps
)
    : source(src),
      mask(msk),
      patchSize(ps)
{
    if(patchSize < 1)
        ps = DEFAULT_PATCH_SIZE;

    msk.convertTo(confidence, CV_64FC1, (double)(1)/WHITE);
}



ExBsdInpaint::~ExBsdInpaint(
) {
}


cv::Mat ExBsdInpaint::start(
    int count = -1
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::start()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    for(int i = count; i != 0; i--) {
        if(i < -1)
            i++;
        std::vector<std::vector<cv::Point> > edge = findEdgeLine(mask);
        if(edge.empty())
            break;
        cv::Point tar = findHighestPriorityPoint(source, mask, confidence, edge);
        cv::Point src = findExemplarPoint(source, mask, tar);
        fillPatch(source, mask, tar, src);
        update(mask, confidence, tar, src);
        std::cout << i << ", tar: "<< tar << ", src: " << src << std::endl;
    }

    return source;
}
