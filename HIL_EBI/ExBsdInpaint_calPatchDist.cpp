#include "ExBsdInpaint.h"

#include <cmath>
#include <iostream>


//=============================================================================
// calculate color distance between two patches (using rgb color space)
// result is the sum of square differences at all pixels in patches.
// ERROR: return value < 0
//=============================================================================
double ExBsdInpaint::calPatchDist(
    const cv::Mat& dat,
    const cv::Mat& mask,
    cv::Point tar,
    cv::Point src
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::calPatchDist()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    double res = 0.0;
    int count = 0;
    cv::Vec3b* d = (cv::Vec3b*)dat.data;
    uchar* m = (uchar*)mask.data;

    for(int i = -patchSize/2; i < (patchSize+1)/2; i++) {
        for(int j = -patchSize/2; j < (patchSize+1)/2; j++) {
            if(m[(tar.y+i)*mask.cols+(tar.x+j)] != EXEMPLAR_REGION
               && m[(src.y+i)*dat.cols+(src.x+j)] != EXEMPLAR_REGION) {
                return -1.0;
            }
            if(m[(tar.y+i)*mask.cols+(tar.x+j)] == EXEMPLAR_REGION) {
                count++;
                res += cv::norm(d[(tar.y+i)*dat.cols+(tar.x+j)],
                                d[(src.y+i)*dat.cols+(src.x+j)]);
            }
        }
    }

    if(count == 0)
        return -1.0;
    else
        return res/count;
}
