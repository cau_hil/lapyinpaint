/*****************************************************************************/
/*   (c) Human Interface Laboratory, Chung-Ang University, Seoul Korea       */
/*                                                                           */
/*   Title      : LaPyInpaint Header file                                    */
/*   File Name  : LaPyInpaint.h                                              */
/*   Author     : Jeongkeun Park                                             */
/*   Date       : April 21, 2015                                             */
/*   Version    :                                                            */
/*   Project    : Exemplar Based Inpaint using Laplacian Pyramid             */
/*   Description: LaPyInpaint by inheritance from ExBsdInpaint Proj          */
/*                                                                           */
/*****************************************************************************/
#ifndef __LA_PY_INPAINT_H__
#define __LA_PY_INPAINT_H__


#include "ExBsdInpaint.h"
#include <opencv/cv.h>


class LaPyInpaint : public ExBsdInpaint
{
public:
	LaPyInpaint(cv::Mat src, cv::Mat msk, int numOfLayer);
	~LaPyInpaint();

	cv::Mat start(int);

protected:
	int numOfLayer;
	cv::Mat *LaplacianImages; // l_0 to l_
	cv::Mat *LaplacianMasks;
	cv::Mat *LaplacianConfs;//confidence
	cv::Mat sourceArea; // s+L
	cv::Mat targetArea; // t+L
	cv::Mat maskArea;
	cv::Mat confArea;
	cv::Point *sourcePoints;
	cv::Point *targetPoints;
	//Confidence table

private:
	void buildPyramid();
	void downSample(const cv::Mat src, cv::Mat& dst, int downLevel);
	void upSample(const cv::Mat src, cv::Mat& dst, int upLevel);
	void downSample(const cv::Mat src, cv::Mat& dst, int width, int height);
	void upSample(const cv::Mat src, cv::Mat& dst, int width, int height);
};


#endif //__LA_PY_INPAINT_H__
