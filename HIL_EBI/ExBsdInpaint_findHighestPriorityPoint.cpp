#include "ExBsdInpaint.h"

#include <iostream>


//=============================================================================
// select a point within the edge array using a value
// which is a product of confidence and data
//=============================================================================
cv::Point ExBsdInpaint::findHighestPriorityPoint(
    const cv::Mat& source,
    const cv::Mat& mask,
    const cv::Mat& confidence,
    std::vector<std::vector<cv::Point> >& edge
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::findHighestPriorityPoint()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    cv::Point maxPoint = cv::Point();
    double maxVal = -1.0;

    for(std::vector<std::vector<cv::Point> >::iterator it = edge.begin(); it != edge.end() ;it++) {
        for(std::vector<cv::Point>::iterator jt = (*it).begin(); jt != (*it).end(); jt++) {
            if(((*jt).x < patchSize/2) || ((*jt).x > source.cols-(patchSize+1)/2) ||
               ((*jt).y < patchSize/2) || ((*jt).y > source.rows-(patchSize+1)/2))
                 continue;

            double val = calPatchConfidence(confidence, (*jt)) * calDataTerm(source, mask, jt, (*it));
            if(maxVal < 0.0 || (maxVal < val && val >= 0.0)) {
                maxVal = val;
                maxPoint = cv::Point((*jt).x, (*jt).y);
            }
        }
    }

    return maxPoint;
}
