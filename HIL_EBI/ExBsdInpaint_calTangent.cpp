#include "ExBsdInpaint.h"

#include <iostream>


cv::Vec2d tangent(int i,
                std::vector<cv::Point>& edge);


cv::Vec2d ExBsdInpaint::calTangent(
    std::vector<cv::Point>::iterator& it,
    std::vector<cv::Point>& edge
) {
#ifdef DEBUG
    std::cout << "==============================================" << std::endl;
    std::cout << "  ExBsdInpaint::calTangent()" << std::endl;
    std::cout << "==============================================" << std::endl;
#endif
    cv::Vec2d prev;
    cv::Vec2d next;
    cv::Vec2d cur(0.0, 0.0);
    int i = std::distance(edge.begin(), it);
    if(edge.size() < 3) {
        return cur;
    }
    if(it == edge.begin()) {
        prev = tangent(edge.size()-1, edge);
    } else {
        prev = tangent(i-1, edge);
    }
    if(it == --edge.end()) {
        next = tangent(0, edge);
    } else {
        next = tangent(i+1, edge);
    }
    cur = tangent(i, edge);

    return (prev + next + 2*cur)/4.0;
}


cv::Vec2d tangent(
    int i,
    std::vector<cv::Point>& edge
    ) {
    int dx = 0, dy = 0;

    if(edge.size() < 3) {
        ;
    } else if(i == 0) {
        dx = edge[i+1].x - edge[edge.size()-1].x;
        dy = edge[i+1].y - edge[edge.size()-1].y;
    } else if(i == edge.size()-1) {
        dx = edge[0].x - edge[i-1].x;
        dy = edge[0].y - edge[i-1].y;
    } else {
        dx = edge[i+1].x - edge[i-1].x;
        dy = edge[i+1].y - edge[i-1].y;
    }

    return cv::Vec2d(dx, dy);
}
