/*****************************************************************************/
/*   (c) Human Interface Laboratory, Chung-Ang University, Seoul Korea       */
/*                                                                           */
/*   Title      : ExBsdInpaint Header file                                   */
/*   File Name  : ExBsdInpaint.h                                             */
/*   Author     : Jeongkeun Park                                             */
/*   Date       : April 21, 2015                                             */
/*   Version    :                                                            */
/*   Project    : Exemplar Based Inpaint                                     */
/*   Description: Exemplar Based Inpaint and parent class of inpaint         */
/*                                                                           */
/*****************************************************************************/
#ifndef __EX_BSD_INPAINT_H__
#define __EX_BSD_INPAINT_H__


#include <opencv2/core/core.hpp>

#include <vector>


const int DEFAULT_PATCH_SIZE = 9;
const uchar EXEMPLAR_REGION = 255;


class ExBsdInpaint
{
public:
    explicit ExBsdInpaint(cv::Mat src, cv::Mat msk, int patchSize = DEFAULT_PATCH_SIZE);
    ~ExBsdInpaint();

    cv::Mat start(int);

protected:
    void fillPatch(cv::Mat& source, const cv::Mat& mask, cv::Point target, cv::Point sourcep);
    std::vector<std::vector<cv::Point> > findEdgeLine(const cv::Mat& mask);
    cv::Point findExemplarPoint(const cv::Mat& source, const cv::Mat& mask, cv::Point);
    cv::Point findHighestPriorityPoint(const cv::Mat& source, const cv::Mat& mask, const cv::Mat& confidence, std::vector<std::vector<cv::Point> >&);
    void update(cv::Mat& mask, cv::Mat& confidence, cv::Point tar, cv::Point src);

    cv::Mat confidence;
    cv::Mat mask;
    int patchSize;
    cv::Mat source;

private:
    double calDataTerm(const cv::Mat& source, const cv::Mat& mask, std::vector<cv::Point>::iterator&, std::vector<cv::Point>&);
    cv::Vec2d calIsophote(const cv::Mat& graySource, const cv::Mat& mask, cv::Point);
    double calPatchConfidence(const cv::Mat& confidence, cv::Point);
    double calPatchDist(const cv::Mat& source, const cv::Mat& mask, cv::Point, cv::Point);
    cv::Vec2d calTangent(std::vector<cv::Point>::iterator&, std::vector<cv::Point>&);
};


#endif
